﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Results;
using System.Web.Mvc;
using PaylocityBenefitinator.Web.Models;

namespace PaylocityBenefitinator.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly BenefitManager _benefitManager;

        public HomeController()
        {
            // Possibly inject as a service?
            _benefitManager = new BenefitManager(new Discount(), 1000, 500, 52);
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Calculate(CalculationSubmission model)
        {
            // Typically use a convention based mapping strategy here
            var employee = new Employee
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Salary = model.Salary
            };
            if (model.Dependants != null && model.Dependants.Any())
            {
                employee.Dependants = model.Dependants.Select(x => new Dependant() { FirstName = x.FirstName, LastName = x.LastName });
            }

            var benefitReport = _benefitManager.Calculate(employee);
            return new JsonResult() { Data = benefitReport };
        }
    }

}

namespace PaylocityBenefitinator.Web.Models
{
    public class CalculationSubmission
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public long Salary { get; set; }
        public IEnumerable<Dependant> Dependants { get; set; }
    }

    public class Dependants
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}