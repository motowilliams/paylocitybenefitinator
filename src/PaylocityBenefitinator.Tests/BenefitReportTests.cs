﻿using System.Collections.Generic;
using Should;
using Xunit;

namespace PaylocityBenefitinator.Tests
{
    public class FullPriceTests
    {
        private Discount discount;
        public FullPriceTests()
        {
            discount = new Discount();
        }

        [Fact]
        public void should_return_1000_for_no_dependants()
        {
            var manager = new BenefitManager(discount, employeeBenefitCost: 1000, dependantBenefitCost: 500, payPeriods: 52);
            var employee = new Employee { FirstName = "John", LastName = "Smith", Salary = 52000 };

            BenefitReport benefitReport = manager.Calculate(employee);

            benefitReport.TotalCost.ShouldEqual(1000);
        }

        [Fact]
        public void should_return_1500_for_1_dependant()
        {
            var manager = new BenefitManager(discount, employeeBenefitCost: 1000, dependantBenefitCost: 500, payPeriods: 52);
            IEnumerable<Dependant> dependants = new List<Dependant> { new Dependant { FirstName = "Timmy", LastName = "Smith" } };
            var employee = new Employee { FirstName = "John", LastName = "Smith", Salary = 52000, Dependants = dependants };

            BenefitReport benefitReport = manager.Calculate(employee);

            benefitReport.TotalCost.ShouldEqual(1500);
        }

        [Fact]
        public void should_return_2500_for_3_dependant()
        {
            var manager = new BenefitManager(discount, employeeBenefitCost: 1000, dependantBenefitCost: 500, payPeriods: 52);
            IEnumerable<Dependant> dependants = new List<Dependant>
            {
                new Dependant { FirstName = "Timmy", LastName = "Smith" },
                new Dependant { FirstName = "Tommy", LastName = "Smith" },
                new Dependant { FirstName = "Tammy", LastName = "Smith" },
            };
            var employee = new Employee { FirstName = "John", LastName = "Smith", Salary = 52000, Dependants = dependants };

            BenefitReport benefitReport = manager.Calculate(employee);

            benefitReport.TotalCost.ShouldEqual(2500);
        }
    }

    public class DiscountTests
    {
        private Discount discount;
        public DiscountTests()
        {
            discount = new Discount();
        }

        [Fact]
        public void should_return_900_for_no_dependants()
        {
            var manager = new BenefitManager(discount, employeeBenefitCost: 1000, dependantBenefitCost: 500, payPeriods: 52);
            var employee = new Employee { FirstName = "Angela", LastName = "Smith", Salary = 52000 };

            BenefitReport benefitReport = manager.Calculate(employee);

            benefitReport.TotalCost.ShouldEqual(900);
        }

        [Fact]
        public void should_return_1350_for_1_dependant()
        {
            var manager = new BenefitManager(discount, employeeBenefitCost: 1000, dependantBenefitCost: 500, payPeriods: 52);
            IEnumerable<Dependant> dependants = new List<Dependant> { new Dependant { FirstName = "Anthony", LastName = "Smith" } };
            var employee = new Employee { FirstName = "Angela", LastName = "Smith", Salary = 52000, Dependants = dependants };

            BenefitReport benefitReport = manager.Calculate(employee);

            benefitReport.TotalCost.ShouldEqual(1350);
        }

        [Fact]
        public void should_return_2250_for_3_dependant()
        {
            var manager = new BenefitManager(discount, employeeBenefitCost: 1000, dependantBenefitCost: 500, payPeriods: 52);
            IEnumerable<Dependant> dependants = new List<Dependant>
            {
                new Dependant { FirstName = "Anthony", LastName = "Smith" },
                new Dependant { FirstName = "Andrew", LastName = "Smith" },
                new Dependant { FirstName = "Allison", LastName = "Smith" },
            };
            var employee = new Employee { FirstName = "Angela", LastName = "Smith", Salary = 52000, Dependants = dependants };

            BenefitReport benefitReport = manager.Calculate(employee);

            benefitReport.TotalCost.ShouldEqual(2250);
        }

        [Fact]
        public void should_return_2350_for_1_discount_dependant_and_2_full_price_dependants()
        {
            var manager = new BenefitManager(discount, employeeBenefitCost: 1000, dependantBenefitCost: 500, payPeriods: 52);
            IEnumerable<Dependant> dependants = new List<Dependant>
            {
                new Dependant { FirstName = "Anthony", LastName = "Smith" },
                new Dependant { FirstName = "James", LastName = "Smith" },
                new Dependant { FirstName = "Jill", LastName = "Smith" },
            };
            var employee = new Employee { FirstName = "Angela", LastName = "Smith", Salary = 52000, Dependants = dependants };

            BenefitReport benefitReport = manager.Calculate(employee);

            benefitReport.TotalCost.ShouldEqual(2350);
        }

        [Fact]
        public void should_return_2450_for_full_price_employee_and_1_discount_dependant_and_2_full_price_dependants()
        {
            var manager = new BenefitManager(discount, employeeBenefitCost: 1000, dependantBenefitCost: 500, payPeriods: 52);
            IEnumerable<Dependant> dependants = new List<Dependant>
            {
                new Dependant { FirstName = "Anthony", LastName = "Smith" },
                new Dependant { FirstName = "James", LastName = "Smith" },
                new Dependant { FirstName = "Jill", LastName = "Smith" },
            };
            var employee = new Employee { FirstName = "John", LastName = "Smith", Salary = 52000, Dependants = dependants };

            BenefitReport benefitReport = manager.Calculate(employee);

            benefitReport.TotalCost.ShouldEqual(2450);
        }
    }
}