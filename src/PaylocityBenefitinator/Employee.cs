using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace PaylocityBenefitinator
{
    public class Employee
    {
        public Employee()
        {
            Dependants = Enumerable.Empty<Dependant>();
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public long Salary { get; set; }
        public IEnumerable<Dependant> Dependants { get; set; } 
    }
}