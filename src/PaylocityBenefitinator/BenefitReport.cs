using System;
using System.Collections.Generic;
using System.Linq;

namespace PaylocityBenefitinator
{
    public class BenefitReport
    {
        public BenefitReport()
        {
            BenefitReportItems = Enumerable.Empty<BenefitReportItem>();
        }

        public decimal TotalCost
        {
            get
            {
                var totalCost = BenefitReportItems.Sum(x => x.EmployeeCostPerPayPeriod)
                    + BenefitReportItems.Select(x => x.DependantPerPayPeriod.Sum(y=>y.BenefitCostPerPayPeriod)).Sum();

                return Decimal.Round(totalCost, 2, MidpointRounding.AwayFromZero);
            }
        }
        public Employee Employee { get; set; }
        public IEnumerable<BenefitReportItem> BenefitReportItems { get; set; }
    }
}