using System.Collections.Generic;

namespace PaylocityBenefitinator
{
    public interface IDiscountStrategy
    {
        BenefitCost Calculate(Employee employee, int payPeriods, decimal employeeAnnualCost, decimal dependantAnnualCost);
    }

    public class BenefitCost
    {
        public decimal EmployeePayPeriodCost { get; set; }
        public List<Dependant> DependantPayPeriodCosts { get; set; }
    }
}