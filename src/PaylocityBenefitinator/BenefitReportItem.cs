using System.Collections.Generic;

namespace PaylocityBenefitinator
{
    public class BenefitReportItem
    {
        public int PayPeriod { get; set; }
        public decimal EmployeeCostPerPayPeriod { get; set; }

        public List<Dependant> DependantPerPayPeriod { get; set; }
    }
}