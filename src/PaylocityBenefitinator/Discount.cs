using System;
using System.Collections.Generic;

namespace PaylocityBenefitinator
{
    public class Discount : IDiscountStrategy
    {
        public decimal Calculate(Employee employee, decimal annualCost)
        {
            if (employee.FirstName.StartsWith("A", StringComparison.OrdinalIgnoreCase)
                || employee.LastName.StartsWith("A", StringComparison.OrdinalIgnoreCase))
            {
                return annualCost - (annualCost * (decimal).10);
            }

            return annualCost;
        }

        public BenefitCost Calculate(Employee employee, int payPeriods, decimal employeeAnnualCost, decimal dependantAnnualCost)
        {
            var percentageDiscount = (decimal).10;

            decimal employeePayPeriodCost;
            var dependantPayPeriodCosts = new List<Dependant>();

            if (employee.FirstName.StartsWith("A", StringComparison.OrdinalIgnoreCase)
                || employee.LastName.StartsWith("A", StringComparison.OrdinalIgnoreCase))
            {
                employeePayPeriodCost = Decimal.Divide(employeeAnnualCost - (employeeAnnualCost * percentageDiscount), payPeriods);
            }
            else
            {
                employeePayPeriodCost = Decimal.Divide(employeeAnnualCost, payPeriods);
            }

            foreach (var dependant in employee.Dependants)
            {
                decimal item;
                if (dependant.FirstName.StartsWith("A", StringComparison.OrdinalIgnoreCase)
                    || dependant.LastName.StartsWith("A", StringComparison.OrdinalIgnoreCase))
                {
                    item = Decimal.Divide(dependantAnnualCost - (dependantAnnualCost * percentageDiscount), payPeriods);
                }
                else
                {
                    item = Decimal.Divide(dependantAnnualCost, payPeriods);
                }

                dependantPayPeriodCosts.Add(new Dependant {BenefitCostPerPayPeriod = item, FirstName = dependant.FirstName, LastName = dependant.LastName});
            }

            return new BenefitCost { EmployeePayPeriodCost = employeePayPeriodCost, DependantPayPeriodCosts = dependantPayPeriodCosts };
        }
    }
}