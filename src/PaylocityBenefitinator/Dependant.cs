using System;

namespace PaylocityBenefitinator
{
    public class Dependant
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public decimal BenefitCostPerPayPeriod { get; set; }
    }
}