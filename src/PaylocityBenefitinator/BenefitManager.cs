using System;
using System.Collections.Generic;
using System.Linq;

namespace PaylocityBenefitinator
{
    public class BenefitManager
    {
        private readonly IDiscountStrategy _discountStrategy;
        private readonly decimal _employeeBenefitCost;
        private readonly decimal _dependantBenefitCost;
        private readonly int _payPeriods;

        public BenefitManager(IDiscountStrategy discountStrategy, decimal employeeBenefitCost, decimal dependantBenefitCost, int payPeriods)
        {
            _discountStrategy = discountStrategy;
            _employeeBenefitCost = employeeBenefitCost;
            _dependantBenefitCost = dependantBenefitCost;
            _payPeriods = payPeriods;
        }

        public BenefitReport Calculate(Employee employee)
        {
            var items = new List<BenefitReportItem>();

            var benefitCost = _discountStrategy.Calculate(employee, _payPeriods, _employeeBenefitCost, _dependantBenefitCost);

            foreach (var payPeriod in Enumerable.Range(1, _payPeriods))
            {
                var item = new BenefitReportItem
                {
                    PayPeriod = payPeriod,
                    EmployeeCostPerPayPeriod = benefitCost.EmployeePayPeriodCost,
                    DependantPerPayPeriod = benefitCost.DependantPayPeriodCosts,
                };

                items.Add(item);
            }

            return new BenefitReport { Employee = employee, BenefitReportItems = items };
        }
    }
}